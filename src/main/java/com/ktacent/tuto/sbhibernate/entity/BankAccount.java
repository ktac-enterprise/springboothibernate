package com.ktacent.tuto.sbhibernate.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * The Class BankAccount.
 * @author alexk
 */
@Entity
@Table(name = "Bank_Account")
public class BankAccount {
 
    /** The id. */
    @Id
    @GeneratedValue
    @Column(name = "id", nullable = false)
    private Long id;
 
    /** The full name. */
    @Column(name = "Full_Name", length = 128, nullable = false)
    private String fullName;
 
    /** The balance. */
    @Column(name = "Balance", nullable = false)
    private double balance;
 
    /**
     * Gets the id.
     *
     * @return the id
     */
    public Long getId() {
        return id;
    }
 
    /**
     * Sets the id.
     *
     * @param id the new id
     */
    public void setId(Long id) {
        this.id = id;
    }
 
    /**
     * Gets the full name.
     *
     * @return the full name
     */
    public String getFullName() {
        return fullName;
    }
 
    /**
     * Sets the full name.
     *
     * @param fullName the new full name
     */
    public void setFullName(String fullName) {
        this.fullName = fullName;
    }
 
    /**
     * Gets the balance.
     *
     * @return the balance
     */
    public double getBalance() {
        return balance;
    }
 
    /**
     * Sets the balance.
     *
     * @param balance the new balance
     */
    public void setBalance(double balance) {
        this.balance = balance;
    }
}